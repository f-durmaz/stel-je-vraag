$(window).on("scroll", function () {
    if ($(this).scrollTop() > 365) {
        $(".header__question-bar").addClass("not-big");
        $(".header__bar").addClass("not-big");
        $(".header__label").addClass("display-none");
        $(".header__title").addClass("display-none");
        $(".header__image").addClass("image-small");
        $(".header__image").removeClass("header__image");
        $(".header__text").addClass("display-none");
        $(".header__nav--btn").addClass("display-none");
        $(".header__bar--btn").addClass("display-none");
        $(".header__input").addClass("input-small");
        $(".header__input").removeClass("header__input");
    }
    else {
        $(".header__question-bar").removeClass("not-big");
        $(".header__bar").removeClass("not-big");
        $(".header__label").removeClass("display-none");
        $(".header__title").removeClass("display-none");
        $(".header__image").removeClass("image-small");
        $(".image-small").addClass("header__image");
        $(".header__text").removeClass("display-none");
        $(".header__nav--btn").removeClass("display-none");
        $(".header__bar--btn").removeClass("display-none");
        $(".header__input").removeClass("input-small");
        $(".input-small").addClass("header__input");
    }
});
